SELECT
    t.calendar_week_number,
    s.time_id,
    t.day_name,
    s.amount_sold AS sales,
    SUM(s.amount_sold) OVER (ORDER BY s.time_id) AS CUM_SUM,
    ROUND(AVG(s.amount_sold) OVER (PARTITION BY t.calendar_week_number, t.day_name ORDER BY s.time_id ROWS BETWEEN 1 PRECEDING AND 1 FOLLOWING), 2) AS CENTERED_3_DAY_AVG
FROM
    sh.sales s
JOIN
    sh.times t ON s.time_id = t.time_id
WHERE
    EXTRACT(YEAR FROM s.time_id) = 1999
    AND t.calendar_week_number IN (49, 50, 51)
    AND (
        (t.day_name = 'Monday' AND t.day_number_in_week <= 2)
        OR
        (t.day_name = 'Friday' AND t.day_number_in_week <= 2)
    )
ORDER BY
    t.calendar_week_number, s.time_id;
